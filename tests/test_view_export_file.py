# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import unittest
from zsnl_case_management_http.views import _shared, export_file


class TestExportFileView(unittest.TestCase):
    def test_export_file_views(self):
        self.assertEqual(
            export_file.ExportFileViews.create_link_from_entity,
            _shared.create_link_from_entity,
        )
