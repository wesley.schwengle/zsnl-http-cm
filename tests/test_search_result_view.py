# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import unittest
from zsnl_case_management_http.views import _shared, search_result


class TestSearchResultViews(unittest.TestCase):
    def test_search_result_view(self):
        self.assertEqual(
            search_result.SearchResultViews.create_link_from_entity,
            _shared.create_link_from_entity,
        )
