# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class AuthorizationViews(JSONAPIEntityView):
    # Move this away in new version, and do this sexyer
    def create_link_from_entity(
        self, entity=None, entity_type=None, entity_id=None
    ):
        return None

    view_mapper = {
        "GET": {
            "get_departments": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_departments",
                "from": {},
            },
            "get_roles": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_roles",
                "from": {
                    "request_params": {
                        "filter[relationships.parent.id]": "filter_parent_uuid"
                    },
                },
            },
        },
    }
