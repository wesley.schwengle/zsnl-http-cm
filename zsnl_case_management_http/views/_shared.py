# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView
from typing import Optional


def _build_case_link(request, entity_id):
    return request.route_path("get_case", _query={"case_uuid": entity_id})


def _build_custom_object_type_link(request, entity_id):
    return request.route_path(
        "get_custom_object_type", _query={"uuid": entity_id}
    )


def _build_persistent_custom_object_type_link(request, entity_id):
    return request.route_path(
        "get_persistent_custom_object_type", _query={"uuid": entity_id}
    )


def _build_custom_object_link(request, entity_id):
    return request.route_path("get_custom_object", _query={"uuid": entity_id})


def _build_employee_link(request, entity_id):
    return request.route_path(
        "get_subject",
        _query={"employee_uuid": entity_id},
    )


def _build_organization_link(request, entity_id):
    return request.route_path(
        "get_subject",
        _query={"organization_uuid": entity_id},
    )


def _build_person_link(request, entity_id):
    return request.route_path(
        "get_subject",
        _query={"person_uuid": entity_id},
    )


def _build_case_type_version_link(request, entity_id):
    return request.route_path(
        "get_case_type_version", _query={"version_uuid": entity_id}
    )


def _build_case_type_link(request, entity_id):
    return request.route_path(
        "get_case_type_active_version", _query={"case_type_uuid": entity_id}
    )


def _build_subject_link(request, entity_id):
    return request.route_path(
        "get_subject",
        _query={"person_uuid": entity_id},
    )


ENTITY_LINK_BUILDERS = {
    "case": _build_case_link,
    "related_case": _build_case_link,
    "custom_object_type": _build_custom_object_type_link,
    "custom_object": _build_custom_object_link,
    "related_object": _build_custom_object_link,
    "employee": _build_employee_link,
    "organization": _build_organization_link,
    "person": _build_person_link,
    "persistent_object_type": _build_persistent_custom_object_type_link,
    "case_type_version": _build_case_type_version_link,
    "case_type": _build_case_type_link,
    "subject": _build_subject_link,
}


def create_link_from_entity(
    self: JSONAPIEntityView, entity=None, entity_type=None, entity_id=None
) -> Optional[str]:
    if entity is not None:
        entity_id = str(entity.entity_id)

    entity_type = (
        entity_type
        or getattr(entity, "result_type", None)  # search result
        or getattr(entity, "type", None)  # related subject
        or entity.entity_type
    )

    try:
        return ENTITY_LINK_BUILDERS[entity_type](self.request, entity_id)
    except KeyError:
        return None
