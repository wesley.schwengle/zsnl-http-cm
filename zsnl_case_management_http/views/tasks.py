# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from minty.cqrs import UserInfo
from minty_pyramid.session_manager import protected_route
from pyramid.httpexceptions import HTTPBadRequest
from typing import Optional

log = logging.getLogger(__name__)


@protected_route("gebruiker")
def get_task_list(request, user_info: UserInfo):
    "Retrieve a list of tasks, filtered by specified criteria"

    raw_page = request.params.get("page", "1")
    raw_page_size = request.params.get("page_size", "100")
    (page, page_size) = _check_paging_limits(
        page=raw_page, page_size=raw_page_size, max_results=10000
    )

    get_task_list_params = {
        **_get_relationship_filter(request),
        **_get_attribute_filter(request),
    }
    get_task_list_params["page"] = page
    get_task_list_params["page_size"] = page_size

    try:
        get_task_list_params["sort"] = request.params["sort"]
    except KeyError:
        # No sort order specified
        pass

    try:
        get_task_list_params["keyword"] = request.params["keyword"]
    except KeyError:
        # No "keyword" parameter specified
        pass

    query_instance = request.get_query_instance(
        "zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )
    task_list = query_instance.get_task_list(**get_task_list_params)

    links = {"self": {"href": f"{request.current_route_path()}"}}
    return {
        "meta": {"api_version": 2},
        "data": [task_list_serializer(task=task) for task in task_list],
        "links": links,
    }


@protected_route("gebruiker")
def create_task(request, user_info: UserInfo):
    try:
        task_uuid = request.json_body["task_uuid"]
        case_uuid = request.json_body["case_uuid"]
        title = request.json_body["title"]
        phase = int(request.json_body["phase"])
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error
    except ValueError as e:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {"title": "Parameter 'phase' is of incorrect type."}
                ]
            }
        ) from e

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    cmd.create_task(
        task_uuid=task_uuid, case_uuid=case_uuid, title=title, phase=phase
    )
    return {"data": {"success": True}}


@protected_route("gebruiker")
def delete_task(request, user_info: UserInfo):
    try:
        task_uuid = request.json_body["task_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error
    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )
    cmd.delete_task(task_uuid=task_uuid)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def update_task(request, user_info: UserInfo):
    try:
        task_uuid = request.json_body["task_uuid"]
        description = request.json_body["description"]
        title = request.json_body["title"]
        due_date = request.json_body["due_date"]
        assignee = request.json_body["assignee"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )
    cmd.update_task(
        task_uuid=task_uuid,
        description=description,
        title=title,
        due_date=due_date,
        assignee=assignee,
    )
    return {"data": {"success": True}}


@protected_route("gebruiker")
def set_task_completion(request, user_info: UserInfo):
    try:
        task_uuid = request.json_body["task_uuid"]
        completed = request.json_body["completed"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )
    cmd.set_completion_on_task(task_uuid=task_uuid, completed=completed)
    return {"data": {"success": True}}


def task_list_serializer(task):
    serialized_task = {
        "type": "task",
        "id": str(task.uuid),
        "meta": {
            "is_editable": task.is_editable,
            "can_set_completion": task.can_set_completion,
        },
        "attributes": {
            "completed": task.completed,
            "description": task.description if task.description else "",
            "due_date": str(task.due_date) if task.due_date else None,
            "title": task.title,
            "phase": task.phase,
        },
        "relationships": {
            "assignee": serialize_assignee(task.assignee),
            "case": {
                "data": {"id": str(task.case["uuid"]), "type": "case"},
                "meta": {
                    "phase": task.phase,
                    "display_number": task.case["id"],
                },
            },
            "case_type": {
                "data": {
                    "id": str(task.case_type["uuid"]),
                    "type": "case_type",
                },
                "meta": {"display_name": task.case_type["title"]},
            },
            "department": {
                "data": {
                    "id": str(task.department["uuid"]),
                    "type": "department",
                },
                "meta": {"display_name": task.department["display_name"]},
            },
        },
    }
    return serialized_task


def serialize_assignee(assignee) -> Optional[dict]:
    if not assignee:
        return None
    assignee = {
        "data": {
            "meta": {"display_name": assignee["display_name"]},
            "id": str(assignee["uuid"]),
            "type": assignee["type"],
        }
    }
    return assignee


def _get_attribute_filter(request):
    filters = {}
    if "filter[attributes.title.contains]" in request.params:
        title_words = request.params["filter[attributes.title.contains]"]
        if not isinstance(title_words, str):
            raise HTTPBadRequest(
                json={
                    "errors": [
                        {
                            "title": "Parameter 'filter[attributes.title.contains]' must be a string"
                        }
                    ]
                }
            )

        filters["title_words"] = title_words.split(",")

    if "filter[attributes.phase]" in request.params:
        try:
            phase = int(request.params["filter[attributes.phase]"])
        except ValueError as e:
            raise HTTPBadRequest(
                json={
                    "errors": [
                        {
                            "title": "Parameter 'filter[attributes.phase]' is not a valid integer"
                        }
                    ]
                }
            ) from e

        filters["phase"] = phase

    if "filter[attributes.completed]" in request.params:
        completed = request.params["filter[attributes.completed]"]
        if not (
            isinstance(completed, str)
            and completed.lower() in ["true", "false"]
        ):
            raise HTTPBadRequest(
                json={
                    "errors": [
                        {
                            "title": "Parameter 'filter[attributes.completed]' is not boolean"
                        }
                    ]
                }
            )

        completed = True if completed.lower() == "true" else False
        filters["completed"] = completed
    return filters


def _get_relationship_filter(request):
    filters = {}

    for relationship_type in {"case", "case_type", "assignee", "department"}:
        filter_param = f"filter[relationships.{relationship_type}.id]"

        if filter_param in request.params:
            filter_value = request.params[filter_param]
            filter_uuids = filter_value.split(",")

            filters[f"{relationship_type}_uuids"] = filter_uuids

    if "filter[relationships.case.number]" in request.params:
        filter_value = request.params["filter[relationships.case.number]"]
        filter_ids = filter_value.split(",")
        try:
            filters["case_ids"] = [int(id) for id in filter_ids]
        except ValueError as e:
            raise HTTPBadRequest(
                json={
                    "errors": [
                        {
                            "title": "Parameter 'filter[attributes.case.number]' may only contain numbers"
                        }
                    ]
                }
            ) from e

    return filters


def _check_paging_limits(page, page_size, max_results):
    try:
        page = int(page)
    except ValueError as e:
        raise HTTPBadRequest(
            json={
                "errors": [{"title": "Parameter page is not a valid integer"}]
            }
        ) from e

    try:
        page_size = int(page_size)
    except ValueError as e:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {"title": "Parameter page_size is not a valid integer"}
                ]
            }
        ) from e

    if page < 1:
        raise HTTPBadRequest(
            json={"errors": [{"title": "'page' must be >= 1"}]}
        )

    if page_size < 1:
        raise HTTPBadRequest(
            json={"errors": [{"title": "'page_size' must be >= 1"}]}
        )

    if page_size > max_results:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {
                        "title": f"Requested page size {page_size} > max size {max_results}."
                    }
                ]
            }
        )

    return (page, page_size)
